package com.brndl.hourplan

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.brndl.hourplan.subjects.AddSubjectActivity
import com.brndl.hourplan.subjects.SubjectItemAdapter
import com.brndl.hourplan.subjects.SubjectManager
import com.brndl.hourplan.subjects.SubjectManagerActivity
import kotlinx.android.synthetic.main.activity_select_subject.*

class SelectSubjectActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_subject)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        subjectRecycler.layoutManager = LinearLayoutManager(this)


        buttonAdd.setOnClickListener {
            val intent = Intent(this, AddSubjectActivity::class.java)
            startActivity(intent)
        }

        setupAdapter()

    }

    fun setupAdapter() {

        noSubjectsAdded.visibility =
            if (SubjectManager.subjectsSize() == 0)
                View.VISIBLE
            else
                View.GONE

        val adapter = SubjectItemAdapter(SubjectManager.getSubjectsForAdapter())

        subjectRecycler.adapter = adapter

        adapter.setOnClickListener(object : SubjectItemAdapter.OnItemClickListener {
            override fun onItemClick(
                position: Int,
                viewHolder: SubjectItemAdapter.SubjectItemViewHolder
            ) {
                val intent = Intent()
                intent.putExtra("result", position)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

        })

    }

    override fun onResume() {
        super.onResume()
        setupAdapter()

    }
}
