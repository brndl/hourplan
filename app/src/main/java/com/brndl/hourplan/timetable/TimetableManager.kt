package com.brndl.hourplan.timetable

import android.content.Context
import android.content.SharedPreferences
import com.brndl.hourplan.R
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import kotlin.collections.ArrayList

data class SubjectItem(var subject: Int, var room: String? = null)

object TimetableManager {

    lateinit var sharedPrefs: SharedPreferences

    var timetable: ArrayList<MutableList<SubjectItem>> =
        arrayListOf(ArrayList(), ArrayList(), ArrayList(), ArrayList(), ArrayList())

    fun setSubjectAtDay(day: Int, index: Int, subject: SubjectItem): Boolean {
        if (timetable.lastIndex < day) return false
        if (timetable[day].lastIndex < day) return false
        timetable[day][index] = subject
        saveData()
        return true
    }

    fun addSubjectAtDay(day: Int, subject: SubjectItem) {
        timetable[day].add(subject)
        saveData()
    }

    fun removeSubjectAtDay(day: Int, position: Int) {
        timetable[day].removeAt(position)
        saveData()
    }

    fun switchSubjects(day: Int, from: Int, to: Int) {
        val list = timetable[day]
        Collections.swap(list, from, to)
        saveData()

    }

    fun subjectAdded(index: Int) {
        for (day in timetable)
            for (subj in day) {
                if (subj.subject >= index)
                    subj.subject += 1
            }
    }


    fun subjectRemoved(index: Int) {
        for (day in timetable)
            for (subj in day) {
                if (subj.subject == index)
                    subj.subject = -1
                if (subj.subject > index)
                    subj.subject -= 1
            }
    }


    fun saveData() {
        val editor = sharedPrefs.edit()
        val gson = Gson()
        val json: String = gson.toJson(timetable)

        editor.putString("TIMETABLE", json)
        editor.apply()
    }

    inline fun <reified T> Gson.fromJson(json: String) =
        this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    fun loadData() {
        val gson = Gson()
        val json = sharedPrefs.getString("TIMETABLE", "")
        if (json != null && json != "") {
            try {
                timetable = gson.fromJson<ArrayList<MutableList<SubjectItem>>>(json)
            } catch (e: Exception) {
                println("couldn't load anything")

            }
        }
    }

    fun indexOfDay(
        dayOfWeek: Int = Calendar.getInstance().get(Calendar.DAY_OF_WEEK),
        returnWeekend: Boolean = false
    ): Int {
        return when (dayOfWeek) {
            Calendar.MONDAY -> 0
            Calendar.TUESDAY -> 1
            Calendar.WEDNESDAY -> 2
            Calendar.THURSDAY -> 3
            Calendar.FRIDAY -> 4
            Calendar.SATURDAY -> if (returnWeekend) 5
            else 0
            Calendar.SUNDAY -> if (returnWeekend) 6
            else 0
            else -> 0
        }
    }

    fun getDaysWithSubject(subject: Int): ArrayList<Int> {
        val returnList = arrayListOf<Int>()
        for (day in 0 until timetable.size)
            for (subj in timetable[day]) {
                if (subj.subject == subject) {
                    returnList.add(day)
                    break
                }
            }
        return returnList
    }

    fun getDateOfDay(dayIndex: Int): Date {
        val todayIndex = indexOfDay(returnWeekend = true)

        val cal = Calendar.getInstance()
        cal.add(Calendar.DATE, dayDifference(todayIndex, dayIndex))
        return cal.time
    }

    fun dayDifference(fromDay: Int, toDay: Int): Int {
        if (toDay < fromDay) {
            return ((toDay + 7) - fromDay)
        }
        return (toDay - fromDay)
    }

    fun moveDateToTheFuture(date: Date, moveToday: Boolean = true): Date {
        val cal = Calendar.getInstance()
        val today = cal.time
        if (
            if (moveToday)
                date <= today
            else
                date < today
        ) {
            Calendar.getInstance()
            cal.time = date
            cal.add(Calendar.DATE, 7)
            return cal.time
        }
        return date
    }

    fun dayDifference(
        fromDay: Date,
        toDay: Date,
        alwaysPositive: Boolean = true,
        skipToday: Boolean = false
    ): Int {
        if (
            if (skipToday)
                toDay <= fromDay
            else
                toDay < fromDay
        )
            return if (alwaysPositive) {

                val cal = Calendar.getInstance()
                cal.time = toDay
                cal.add(Calendar.DATE, 7)
                (dayDifference(fromDay, cal.time))

            } else
                -1
        var returnDays = 0
        val cal = Calendar.getInstance()
        cal.time = fromDay
        while (cal.time.before(toDay)) {
            cal.add(Calendar.DATE, 1)
            returnDays += 1
        }
        return returnDays
    }

    fun dayDifference(
        toDate: Date,
        alwaysPositive: Boolean = true,
        skipToday: Boolean = false
    ): Int {
        val fromDate: Date = Calendar.getInstance().time
        return dayDifference(fromDate, toDate, alwaysPositive, skipToday)
    }

    fun dayDifferenceString(context: Context?, difference: Int): String? {
        return when (difference) {
            0 -> context?.getString(R.string.today)
            1 -> context?.getString(R.string.tomorrow)
            else -> context?.getString(R.string.in_days_format)?.format(difference)
        }

    }

    fun dayDifferenceString(
        context: Context?, fromDay: Date,
        toDay: Date,
        alwaysPositive: Boolean = true,
        skipToday: Boolean = false
    ): String? {
        return dayDifferenceString(
            context,
            dayDifference(fromDay, toDay, alwaysPositive, skipToday)
        )
    }

    fun dayDifferenceString(
        context: Context?, fromDay: Int, toDay: Int
    ): String? {
        return dayDifferenceString(context, dayDifference(fromDay, toDay))
    }

    fun dayDifferenceString(
        context: Context?, toDay: Date
    ): String? {
        return dayDifferenceString(context, dayDifference(toDay))
    }

    fun dayString(context: Context?, dayOfWeek: Int, short: Boolean = false): String? {
        when (dayOfWeek) {
            0 -> return if (short) context?.getString(R.string.monday_short)
            else context?.getString(R.string.monday)
            1 -> return if (short) context?.getString(R.string.tuesday_short)
            else context?.getString(R.string.tuesday)
            2 -> return if (short) context?.getString(R.string.wednesday_short)
            else context?.getString(R.string.wednesday)
            3 -> return if (short) context?.getString(R.string.thursday_short)
            else context?.getString(R.string.thursday)
            4 -> return if (short) context?.getString(R.string.friday_short)
            else context?.getString(R.string.friday)
            5 -> return if (short) context?.getString(R.string.saturday_short)
            else context?.getString(R.string.saturday)
            6 -> return if (short) context?.getString(R.string.sunday_short)
            else context?.getString(R.string.sunday)

        }
        return "day not found D:"
    }

    fun dayString(context: Context?, date: Date, short: Boolean = false): String? {
        val cal = Calendar.getInstance()
        cal.time = date
        val dayOfWeek: Int = indexOfDay(cal.get(Calendar.DAY_OF_WEEK))
        return dayString(context, dayOfWeek, short)
    }
}