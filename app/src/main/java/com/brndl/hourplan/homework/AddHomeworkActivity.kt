package com.brndl.hourplan.homework

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.brndl.hourplan.R
import com.brndl.hourplan.TextItem
import com.brndl.hourplan.TextItemAdapter
import com.brndl.hourplan.timetable.TimetableManager
import kotlinx.android.synthetic.main.activity_add_homework.*
import kotlinx.android.synthetic.main.activity_homework.toolbar
import java.util.*

class AddHomeworkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_homework)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val subject = intent.getIntExtra("subject", -1)
        val days = intent.getIntegerArrayListExtra("days")


        val dates: ArrayList<Date> = arrayListOf()

        val textItemDays = arrayListOf<TextItem>()
        if (days != null) {
            for (day in days) {
                val cal = Calendar.getInstance()
                val today = cal.time
                cal.set(Calendar.DAY_OF_WEEK, day + 2)
                var targetDay = cal.time
                targetDay = TimetableManager.moveDateToTheFuture(targetDay)
                val textItem = TextItem(
                    TimetableManager.dayString(this, day) ?: "",
                    TimetableManager.dayDifferenceString(this, today, targetDay)
                )

                if (dates.size == 0) {
                    textItemDays.add(textItem)
                    dates.add(targetDay)
                } else {
                    var dateAdded = false
                    for (i in 0 until dates.size) {
                        val date = dates[i]
                        if (targetDay.after(date)) {
                            textItemDays.add(i + 1, textItem)
                            dates.add(i + 1, targetDay)
                            dateAdded = true
                        }
                    }
                    if (!dateAdded) {
                        textItemDays.add(0, textItem)
                        dates.add(0, targetDay)
                    }
                }
            }
            selectedDate = dates[0]
        }

        selectdayRecyclerview.layoutManager = LinearLayoutManager(this)
        val adapter = TextItemAdapter(textItemDays)
        println("adapter: $adapter")
        selectdayRecyclerview.adapter = adapter

        adapter.setOnClickListener(object : TextItemAdapter.OnItemClickListener {
            override fun onItemClick(
                position: Int,
                viewHolder: TextItemAdapter.TextItemViewHolder
            ) {
                val date = dates[position]
                selectedDate = date
            }
        })

        buttonAdd.setOnClickListener {
            val date = selectedDate
            if(date != null)
                if(editTextTask.text.isNotEmpty()) {
                    HomeworkManager.addHomework(date, subject, editTextTask.text.toString())
                    finish()
                }
        }

    }

    var selectedDate: Date? = null
    set(value) {
        field = value
        val dayString = TimetableManager.dayString(this@AddHomeworkActivity, value!!)
        val differenceString = TimetableManager.dayDifferenceString(
            this@AddHomeworkActivity,
            value!!
        )
        textViewDay.text = dayString
        textViewRelativeDay.text = differenceString
    }
}
