package com.brndl.hourplan.homework

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.brndl.hourplan.R
import com.brndl.hourplan.SelectSubjectActivity
import com.brndl.hourplan.timetable.TimetableManager
import kotlinx.android.synthetic.main.activity_homework.*
import kotlinx.android.synthetic.main.activity_learning.addButton
import kotlinx.android.synthetic.main.activity_learning.toolbar

class HomeworkActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homework)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        addButton.setOnClickListener {
            val intent = Intent(this, SelectSubjectActivity::class.java)
            startActivityForResult(intent, 1)
        }

        recyclerViewHomework.layoutManager = LinearLayoutManager(this)
        adapter = HomeworkAdapter(HomeworkManager.homework)
        recyclerViewHomework.adapter = adapter

    }

    lateinit var adapter: HomeworkAdapter

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(data != null) {
            if (requestCode == 1) {
                val subject = data?.getIntExtra("result", -1)
                val intent = Intent(this, AddHomeworkActivity::class.java)
                intent.putExtra("subject", subject)
                intent.putExtra("days", TimetableManager.getDaysWithSubject(subject))
                startActivity(intent)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        adapter.notifyDataSetChanged()
    }
}
