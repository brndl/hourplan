package com.brndl.hourplan.homework

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.brndl.hourplan.R
import com.brndl.hourplan.subjects.SubjectManager
import com.brndl.hourplan.timetable.TimetableManager
import kotlinx.android.synthetic.main.subject_item.view.*

class HomeworkAdapter(val homework: ArrayList<HomeworkManager.Homework>) :
    RecyclerView.Adapter<HomeworkAdapter.HomeworkViewHolder>() {


    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeworkViewHolder {
        context = parent.context
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.subject_item, parent, false)
        return HomeworkViewHolder(view)
    }

    override fun getItemCount(): Int {
        return homework.size
    }

    override fun onBindViewHolder(holder: HomeworkViewHolder, position: Int) {
        val work = homework[position]
        holder.taskText.text = work.task
        holder.subjectText.text = SubjectManager.getSubject(work.subject)?.name
        holder.untilText.text = TimetableManager.dayDifferenceString(context, work.date)
        val color = SubjectManager.getSubject(work.subject)?.color
        if (color != null)
            holder.cardView.setCardBackgroundColor(color)
    }

    class HomeworkViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.cardView
        val taskText: TextView = itemView.subjectText
        val untilText: TextView = itemView.roomText
        val subjectText: TextView = itemView.teacherText
    }

}