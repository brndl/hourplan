package com.brndl.hourplan.homework

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*
import kotlin.collections.ArrayList

object HomeworkManager {

    data class Homework(val date: Date, val subject: Int, val task: String)

    var homework: ArrayList<Homework> = arrayListOf()

    lateinit var sharedPrefs : SharedPreferences

    fun addHomework(date: Date, subject: Int, task: String) {
        if(homework.size == 0){
            homework.add(Homework(date, subject, task))
            return
        }
        for(i in 0 until homework.size) {
            val h = homework[i]
            if(h.date.after(date)){
                homework.add(i, Homework(date, subject, task))
                return
            }
        }
        homework.add(Homework(date, subject, task))
        saveData()
    }

    fun saveData() {
        val editor = sharedPrefs.edit()
        val gson = Gson()
        val json: String = gson.toJson(homework)

        editor.putString("HOMEWORK", json)
        editor.apply()
    }

    inline fun <reified T> Gson.fromJson(json: String) =
        this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    fun loadData() {
        val gson = Gson()
        val json = sharedPrefs.getString("HOMEWORK", "")
        if (json != null && json != "") {
            try {
                homework = gson.fromJson<ArrayList<Homework>>(json)
            } catch (e: Exception) {
                println("couldn't load anything")

            }
        }
    }

}